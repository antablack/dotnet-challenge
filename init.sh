docker-compose up -d

sleep 20

start_dotnet(){
    kill $(lsof -t -i:5001)
    cd dotnet-challenge && dotnet restore && dotnet run
}
start_dotnet_bot(){
    cd dotnet-challenge-bot && dotnet restore && dotnet run
}

start_dotnet & start_dotnet_bot

