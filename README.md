# DotNet Challenge

### Installation
1. git clone https://bitbucket.org/antablack/dotnet-challenge.git
2. Ensure install [DotNet SDK](https://dotnet.microsoft.com/download) and [Docker](https://www.docker.com/products/docker-engine)
3. From the terminal navigate to the folder and run **sh init.sh** (Mac o Linux)
4. Open https://localhost:5001 and create a new account and sign in
![Login](./login.png)

### Documentation
In the chat room you can send messages to all user registered and also you can get stock quote from https://stooq.com/ writing the command **/stock=stock_code** and it will return **<stock_code> quote is $<value> per share**, Example:
![Example 1](./get_stock.png)

You can only send one command per message, If you have a stock code with spaces(**WIG20 Fut**) the syntax must be **/stock="stock_code"** 
![Example 2](./command_example.png)

If there is a invalid code it will return **The stock quotes for <stock_code> could not be obtained**
![Example 3](./invalid_command.png)


