using System;
using System.Security.Cryptography;
using System.Text;
using JWT.Algorithms;
using JWT.Builder;
using dotnet_challenge.Models;
using System.Collections.Generic;
using JWT;
using System.Linq;
using JWT.Serializers;
using System.Text.RegularExpressions;

namespace dotnet_challenge
{
    class Utils
    {
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        public static string Jwt(string userName, string password)
        {
            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);
            var payload = new Dictionary<string, object>
            {
                { "userName", userName},
                { "password", password}
            };
            var token = encoder.Encode(payload, Environment.JWT_SECRET);
            return token;
        }
        public static bool isValidJwt(string token, DotNetChallengeContext context)
        {
            try
            {
                var payload = Utils.decodeJwt(token);
                string userName = (string)payload["userName"];
                string password = (string)payload["password"];
                bool exits = context.Users.Any(b => b.userName == userName && b.password == password);
                if (exits)
                {
                    return true;
                }
                return false;
            }
            catch (TokenExpiredException)
            {
                Console.WriteLine("Token has expired");
                return false;
            }
            catch (SignatureVerificationException)
            {
                Console.WriteLine("Token has invalid signature");
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
        public static IDictionary<string, object> decodeJwt(string token)
        {
            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);
            return decoder.DecodeToObject<IDictionary<string, object>>(token, Environment.JWT_SECRET, verify: true);
        }

        public static User getUserFromJwt(string token, DotNetChallengeContext context)
        {
            try
            {
                var payload = Utils.decodeJwt(token);
                string userName = (string)payload["userName"];
                string password = (string)payload["password"];
                User user = context.Users.Where(b => b.userName == userName && b.password == password).FirstOrDefault();
                return user;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        public static dynamic parseMesg(string mesg)
        {
            Regex regex = new Regex("(\\/stock\\=)(\\\".*\\\"|[^\\s]+)");
            Match match = regex.Match(mesg);
            if (match.Success)
            {
                return new { mesg = mesg, command = match.Groups[2].Value.Replace("\"", "") };
            }
            return new { mesg = mesg, command = "" };
        }
    }
}