using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using dotnet_challenge.Models;
using Microsoft.AspNetCore.Http;
namespace dotnet_challenge.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public class LoginModel : PageModel
    {
        private readonly DotNetChallengeContext _context;
        public LoginModel(DotNetChallengeContext context)
        {
            _context = context;
        }
        [BindProperty]
        public User signIn { get; set; }
        [BindProperty]
        public User signUp { get; set; }
        public string messageSingUp { get; set; } = String.Empty;
        public string messageSingIn { get; set; } = String.Empty;

        public void OnGet()
        {
        }
        public async Task<IActionResult> OnPostSignInAsync()
        {
            var user = _context.Users.Where(b => b.userName == signIn.userName && b.password == Utils.CreateMD5(signIn.password)).FirstOrDefault();
            if (user == null)
            {
                messageSingIn = "Invalid credentials";
                return Page();
            }
            messageSingIn = String.Empty;
            HttpContext.Session.SetString("authorization", Utils.Jwt(user.userName, user.password));
            return RedirectToPage("/index");
        }
        public async Task<IActionResult> OnPostSignUpAsync()
        {
            signUp.password = Utils.CreateMD5(signUp.password);
            _context.Users.Add(signUp);
            await _context.SaveChangesAsync();
            messageSingUp = "Successful registration";
            return Page();
        }
    }
}
