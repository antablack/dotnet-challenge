﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Http;
using dotnet_challenge.Models;
namespace dotnet_challenge.Pages
{
    public class IndexModel : PageModel
    {
        private readonly DotNetChallengeContext _context;
        public List<Message> messages;
        public IndexModel(DotNetChallengeContext context)
        {
            _context = context;
            messages = new List<Message>();
        }
        public string accessToken { get; set; } = "";
        public IActionResult OnGet()
        {
            if (HttpContext.Session.GetString("authorization") == null)
            {
                return RedirectToPage("/login");
            }
            accessToken = HttpContext.Session.GetString("authorization");
            if(!Utils.isValidJwt(accessToken, _context)){
                HttpContext.Session.SetString("authorization", "");
                return RedirectToPage("/login");
            }
            messages = (from m in _context.Messages
                        join u in _context.Users on m.userId equals u.userId
                        orderby m.timeStamp descending
                        select new Message
                        {
                            messageId = m.messageId,
                            content = m.content,
                            user = new User() { fullName = u.fullName }
                        }).Take(50).ToList();
            messages.Reverse();
            return new PageResult();
        }
    }
}
