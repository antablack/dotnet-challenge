using System;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;
using dotnet_challenge.Models;
using dotnet_challenge.Services;
namespace dotnet_challenge.Hubs
{
    public class ChatHub : Hub
    {
        public static ChatHub instance;
        ChatHub()
        {
            instance = this;
        }
        private readonly DotNetChallengeContext _context;
        public ChatHub(DotNetChallengeContext context)
        {
            _context = context;
        }
        public async Task SendMessage(string token, string mesg)
        {
            User user = Utils.getUserFromJwt(token, _context);
            if (user != null)
            {
                var result = Utils.parseMesg(mesg);
                string command = (string)result.command;
                mesg = (string)result.mesg;
                if (mesg != String.Empty)
                {
                    await Clients.All.SendAsync("ReceiveMessage", user.fullName, mesg);
                    Message message = new Message();
                    message.userId = user.userId;
                    message.content = mesg;
                    message.timeStamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    _context.Messages.Add(message);
                    _context.SaveChanges();
                }

                if (command != String.Empty)
                {
                    DotNetBot.getInstance().getStock(command);
                }
            }
            else
            {
                await Clients.All.SendAsync("ReceiveMessage", null, "");
            }
        }
    }
}