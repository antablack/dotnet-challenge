namespace dotnet_challenge
{
    class Environment
    {
        public static string CONNECTION_STRING { get; } = "server=127.0.0.1;port=3306;database=dotnet_challange;userid=root;password=N31v4";
        public static string RABBITMQ_USER { get; } = "admin";

        public static string RABBITMQ_PASS { get; } = "N31v4";

        public static int RABBITMQ_PORT { get; } = 5672;

        public static string RABBITMQ_HOST { get; } = "localhost";
        public static string JWT_SECRET { get; } = "A9YZZFgRcU";

    }

}