CREATE DATABASE dotnet_challange;
USE dotnet_challange;
CREATE TABLE `Users` (
  `userId` INT NOT NULL AUTO_INCREMENT,
  `userName` VARCHAR(400) NOT NULL,
  `fullName` VARCHAR(200) NOT NULL,
  `password` VARCHAR(200) NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`userId`));

  CREATE TABLE `Messages` (
  `messageId` INT NOT NULL AUTO_INCREMENT,
  `userId`  INT NOT NULL,
  `content` VARCHAR(500) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL,
  PRIMARY KEY (`messageId`));

