using System;
using System.Text;
using dotnet_challenge.Hubs;
using dotnet_challenge.Models;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace dotnet_challenge.Services
{
    class DotNetBot
    {
        IConnection connection;
        IModel channel;
        private static DotNetBot instance;

        public static DotNetBot getInstance()
        {
            if (instance == null)
            {
                instance = new DotNetBot();
            }
            return instance;
        }
     
        DotNetBot()
        {
            var factory = new ConnectionFactory()
            {
                UserName = Environment.RABBITMQ_USER,
                Password = Environment.RABBITMQ_PASS,
                HostName = Environment.RABBITMQ_HOST,
                Port = Environment.RABBITMQ_PORT
            };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: "setStock", type: "fanout");
        }

        public void getStock(string stockCode)
        {
            var body = Encoding.UTF8.GetBytes(stockCode);
            channel.BasicPublish(exchange: "getStock",
                                 routingKey: "getStock",
                                 basicProperties: null,
                                 body: body);
            Console.WriteLine(" [x] Sent {0}", stockCode);
        }
        public void suscribe()
        {
            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName,
                              exchange: "setStock",
                              routingKey: "setStock");

            Console.WriteLine(" [*] Waiting for Events for Bot");
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                var body = Encoding.UTF8.GetString(ea.Body);
                await Startup.ConnectionHub.Clients.All.SendAsync("ReceiveMessage", "Bot", body);
            };
            channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }
    }
}