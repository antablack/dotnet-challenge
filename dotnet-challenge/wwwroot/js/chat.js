"use strict";
function initializeSignalR(accessToken){
    const connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

    document.getElementById("sendButton").disabled = true;
    
    connection.on("ReceiveMessage", function (user, message) {
        if(!user){
            return;
        }
        if(document.getElementById("empty-state")){
            document.getElementById("empty-state").style.display = "none";
        }
        let msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        
        let template = `
            <div class="mesg">
                <span>${user}</span>
                <p>${message}</p>
            </div>
        `
        document.getElementById("container-mesgs").innerHTML += template;
        document.getElementById("container-mesgs").scrollTo(0, document.getElementById("container-mesgs").scrollHeight);
    });
    
    connection.start().then(function(){
        document.getElementById("sendButton").disabled = false;
    }).catch(function (err) {
        return console.error(err.toString());
    });
    
    document.getElementById("sendButton").addEventListener("click", function (event) {
        let message = document.getElementById("mesgTextBox").value;
        connection.invoke("SendMessage", accessToken, message).catch(function (err) {
            return console.error(err.toString());
        });
        document.getElementById("mesgTextBox").value = "";
        event.preventDefault();
    });
}