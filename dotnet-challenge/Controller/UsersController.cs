using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dotnet_challenge.Models;
using Microsoft.AspNetCore.Mvc;
using dotnet_challenge.Services;

namespace dotnet_challenge.Controllers {

    [Route ("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase {
        private readonly DotNetChallengeContext _context;
        public UsersController (DotNetChallengeContext context) {
            _context = context;
        }
        [HttpGet]
        public object Get () {
            DotNetBot.getInstance().getStock("aapl.us");
            return _context.Users;
        }
    }
}