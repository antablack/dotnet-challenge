using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace dotnet_challenge.Models
{
    public class Message
    {
        public int messageId { get; set; }
        public int userId { get; set; }
        public string content { get; set; }
        public String timeStamp { get; set; }
        public User user { get; set; } = new User();
    }
}