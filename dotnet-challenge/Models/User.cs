using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
namespace dotnet_challenge.Models
{
    public class User
    {
        public int userId { get; set; }
        public string fullName { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string type { get; set; } = "STANDARD";
        public List<Message> messages { get; set; }
    }
}