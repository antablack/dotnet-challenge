using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using dotnet_challenge.Models;
namespace dotnet_challenge.Models
{
    public class DotNetChallengeContext : DbContext
    {
        public DotNetChallengeContext(DbContextOptions<DotNetChallengeContext> options)
            : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
    }
}