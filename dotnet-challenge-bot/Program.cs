﻿using System;
using System.Net;
using System.Text;
using Microsoft.AspNet.SignalR.Client;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
namespace dotnet_challenge_bot
{
    class Program
    {
        static void Main(string[] args)
        {
            DotNetBot.getInstance().suscribe();
            Console.WriteLine(" Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
