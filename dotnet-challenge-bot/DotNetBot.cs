using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace dotnet_challenge_bot
{
    class DotNetBot
    {
        IConnection connection;
        IModel channel;
        private static DotNetBot instance;
        public static DotNetBot getInstance()
        {
            if (instance == null)
            {
                instance = new DotNetBot();
            }
            return instance;
        }

        DotNetBot()
        {
            var factory = new ConnectionFactory()
            {
                UserName = "admin",
                Password = "N31v4",
                HostName = "localhost",
                Port = 5672
            };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: "getStock", type: "fanout");
        }

        public void sendStock(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: "setStock",
                                 routingKey: "setStock",
                                 basicProperties: null,
                                 body: body);
        }
        public void suscribe()
        {
            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName,
                              exchange: "getStock",
                              routingKey: "getStock");

            Console.WriteLine(" [*] Waiting for events");
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var stockCode = Encoding.UTF8.GetString(body);
                var row = Utils.getCSV(String.Format("https://stooq.com/q/l/?s={0}&f=sd2t2ohlcv&h&e=csv", stockCode));
                var stock = Utils.getStock(row);
                var message = String.Empty;
                if (stock == "N/D")
                {
                    message = String.Format("The stock quotes for {0} could not be obtained", stockCode.ToUpper());
                }
                else
                {
                    message = String.Format("{0} quote is ${1} per share", stockCode.ToUpper(), stock);
                }
                sendStock(message);
                Console.WriteLine(" [x]----{0}", message);
            };
            channel.BasicConsume(queue: queueName,
                                 autoAck: true,
                                 consumer: consumer);
        }

    }
}