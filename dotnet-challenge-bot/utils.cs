using System;
using System.IO;
using System.Net;

namespace dotnet_challenge_bot
{
    class Utils
    {
        public static string getCSV(string url)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            StreamReader sr = new StreamReader(resp.GetResponseStream());
            sr.ReadLine();
            string results = sr.ReadToEnd();
            sr.Close();
            return results;
        }
        public static string getStock(string row){
            return row.Split(",")[6];
        }
    }
}